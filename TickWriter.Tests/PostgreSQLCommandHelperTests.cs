﻿using TickWriter;
using System;
using Xunit;
using TickWriterLibrary;

namespace TickWriter.Tests
{
    public class PostgreSQLCommandHelperTests
    {
        [Fact]
        public void GetDBExistCheckCmdStr_ShouldWork()
        {
            // Arrange
            string dbName = "TestDatabase";
            string expected = $@"SELECT 1 FROM pg_database WHERE datname = '{dbName}'";

            // Act
            string actual = PostgreSQLCommandHelper.GetDBExistCheckCmdStr(dbName);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCreateDBCmdStr_ShouldWork()
        {
            // Arrange
            string dbName = "TestDatabase";
            string userId = "TestUser";
            string expected = $@"CREATE DATABASE {dbName} WITH 
                                        OWNER = {userId}
                                        ENCODING = 'UTF8'
                                        CONNECTION LIMIT = -1;

                                    COMMENT ON DATABASE {dbName} IS '{dbName} database for TimescaleDB.';";

            // Act
            string actual = PostgreSQLCommandHelper.GetCreateDBCmdStr(dbName, userId);

            // Assert 
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDBConversionCmdStr_ShouldWork()
        {
            // Arrange
            string expected = @"CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;";

            // Act
            string actual = PostgreSQLCommandHelper.GetDBConversionCmdStr();

            // Assert 
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetHyperTableConversionCmdStr_ShouldWork()
        {
            // Arrange
            string tableName = "TestTable";
            string expected = $@"SELECT create_hypertable('{tableName}', 'time');";

            // Act
            string actual = PostgreSQLCommandHelper.GetHyperTableConversionCmdStr(tableName);

            // Assert 
            Assert.Equal(expected, actual);
        }






    }
}
