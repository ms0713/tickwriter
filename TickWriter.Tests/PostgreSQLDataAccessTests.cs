﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TickWriterLibrary;
using Xunit;

namespace TickWriter.Tests
{
    public class PostgreSQLDataAccessTests
    {
        [Fact]
        public void TableSchema_CountCheck()
        {
            // Arrange 
            int expected = 50;

            // Act
            int actual = PostgreSQLDataAccess.TableSchemaTest.Count;

            // Assert
            Assert.Equal(expected, actual);

        }


        [Theory]
        [InlineData("time")]
        [InlineData("Mode")]
        [InlineData("InstrumentToken")]
        [InlineData("Tradable")]
        [InlineData("LastPrice")]
        [InlineData("LastQuantity")]
        [InlineData("AveragePrice")]
        [InlineData("Volume")]
        [InlineData("BuyQuantity")]
        [InlineData("SellQuantity")]
        [InlineData("Open")]
        [InlineData("High")]
        [InlineData("Low")]
        [InlineData("Close")]
        [InlineData("Change")]
        [InlineData("Bid1Quantity")]
        [InlineData("Bid1Price")]
        [InlineData("Bid1Orders")]
        [InlineData("Bid2Quantity")]
        [InlineData("Bid2Price")]
        [InlineData("Bid2Orders")]
        [InlineData("Bid3Quantity")]
        [InlineData("Bid3Price")]
        [InlineData("Bid3Orders")]
        [InlineData("Bid4Quantity")]
        [InlineData("Bid4Price")]
        [InlineData("Bid4Orders")]
        [InlineData("Bid5Quantity")]
        [InlineData("Bid5Price")]
        [InlineData("Bid5Orders")]
        [InlineData("Offer1Quantity")]
        [InlineData("Offer1Price")]
        [InlineData("Offer1Orders")]
        [InlineData("Offer2Quantity")]
        [InlineData("Offer2Price")]
        [InlineData("Offer2Orders")]
        [InlineData("Offer3Quantity")]
        [InlineData("Offer3Price")]
        [InlineData("Offer3Orders")]
        [InlineData("Offer4Quantity")]
        [InlineData("Offer4Price")]
        [InlineData("Offer4Orders")]
        [InlineData("Offer5Quantity")]
        [InlineData("Offer5Price")]
        [InlineData("Offer5Orders")]
        [InlineData("LastTradeTime")]
        [InlineData("OI")]
        [InlineData("OIDayHigh")]
        [InlineData("OIDayLow")]
        [InlineData("Timestamp")]
        public void TableSchema_ColumnCheck(string paramName)
        {
            // Arrange 

            // Act

            // Assert
            Assert.Contains<string>(paramName, PostgreSQLDataAccess.TableSchemaTest.Keys);

        }
    }
}
