﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KiteTickerLibrary
{
    public class Constants
    {
        // Ticker modes
        public const string MODE_FULL = "full";
        public const string MODE_QUOTE = "quote";
        public const string MODE_LTP = "ltp";

    }
}
