﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KiteTickerLibrary
{
    public enum Instruments
    {
        Equity,
        Future
    }

    public enum InstrumentType
    {
        EQ,
        FUT
    }

    public enum Exchange
    {
        NSE,
        NFO
    }
}
