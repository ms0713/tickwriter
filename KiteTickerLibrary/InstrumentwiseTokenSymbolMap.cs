﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiteTickerLibrary
{
    public class InstrumentwiseTokenSymbolMap
    {
        /// <summary>
        /// Instrument wise Instrument Token - Trading Symbol mapping for NSE
        /// </summary>
        public static Dictionary<string, Dictionary<uint, string>> InstrumentwiseTokenSymbolMapNSE = new Dictionary<string, Dictionary<uint, string>>();

        /// <summary>
        /// Returns symbol name from token
        /// </summary>
        /// <param name="token">Token number of Symbol</param>
        /// <returns></returns>
        public static string GetTableName(uint token)
        {
            // Look in Equities list
            if (InstrumentwiseTokenSymbolMapNSE[Instruments.Equity.ToString()].TryGetValue(token, out string equity))
                return equity;

            // Look in Futures list
            if (InstrumentwiseTokenSymbolMapNSE[Instruments.Future.ToString()].TryGetValue(token, out string future))
                return future;


            return string.Empty;
        }
    }
}
