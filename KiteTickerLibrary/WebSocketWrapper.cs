﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace KiteTickerLibrary
{
    /// <summary>
    /// A wrapper for .Net's ClientWebSocket with callbacks
    /// </summary>
    internal class WebSocketWrapper
    {
        // Instance of built in ClientWebSocket
        ClientWebSocket m_WebSocket;
        string m_Url;
        int m_BufferLength; // Length of buffer to keep binary chunk

        // Delegates for Web Socket Events
        public delegate void OnConnectHandler();
        public delegate void OnCloseHandler();
        public delegate void OnErrorHandler(string Message);
        public delegate void OnDataHandler(byte[] Data, int Count, WebSocketMessageType MessageType);

        // Web Socket Events 
        public event OnConnectHandler OnConnect;
        public event OnCloseHandler OnClose;
        public event OnDataHandler OnData;
        public event OnErrorHandler OnError;

        /// <summary>
        /// Initialize WebSocket class
        /// </summary>
        /// <param name="Url">Url to the WebSocket.</param>
        /// <param name="BufferLength">Size of buffer to keep byte stream chunk.</param>
        public WebSocketWrapper(string Url, int BufferLength = 2000000)
        {
            m_Url = Url;
            m_BufferLength = BufferLength;
        }

        /// <summary>
        /// Check if WebSocket is connected or not
        /// </summary>
        /// <returns>True if connection is live</returns>
        public bool IsConnected()
        {
            if (m_WebSocket is null)
                return false;

            return m_WebSocket.State == WebSocketState.Open;
        }

        /// <summary>
        /// Connect to WebSocket
        /// </summary>
        public void Connect(Dictionary<string, string> headers = null)
        {
            try
            {
                // Initialize ClientWebSocket instance and connect with Url
                m_WebSocket = new ClientWebSocket();
                if (headers != null)
                {
                    foreach (string key in headers.Keys)
                    {
                        m_WebSocket.Options.SetRequestHeader(key, headers[key]);
                    }
                }
                m_WebSocket.ConnectAsync(new Uri(m_Url), CancellationToken.None).Wait();
            }
            catch (AggregateException e)
            {
                foreach (string ie in e.InnerException.Messages())
                {
                    OnError?.Invoke("Error while connecting. Message: " + ie);
                    if (ie.Contains("Forbidden") && ie.Contains("403"))
                    {
                        OnClose?.Invoke();
                    }
                }
                return;
            }
            catch (Exception e)
            {
                OnError?.Invoke("Error while connecting. Message:  " + e.Message);
                return;
            }
            OnConnect?.Invoke();

            byte[] buffer = new byte[m_BufferLength];
            Action<Task<WebSocketReceiveResult>> callback = null;

            try
            {
                //Callback for receiving data
                callback = t =>
                {
                    try
                    {
                        byte[] tempBuff = new byte[m_BufferLength];
                        int offset = t.Result.Count;
                        bool endOfMessage = t.Result.EndOfMessage;
                        // if chunk has even more data yet to recieve do that synchronously
                        while (!endOfMessage)
                        {
                            WebSocketReceiveResult result = m_WebSocket.ReceiveAsync(new ArraySegment<byte>(tempBuff), CancellationToken.None).Result;
                            Array.Copy(tempBuff, 0, buffer, offset, result.Count);
                            offset += result.Count;
                            endOfMessage = result.EndOfMessage;
                        }
                        // send data to process
                        OnData?.Invoke(buffer, offset, t.Result.MessageType);
                        // Again try to receive data
                        m_WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None).ContinueWith(callback);
                    }
                    catch (Exception e)
                    {
                        if (IsConnected())
                            OnError?.Invoke("Error while recieving data. Message:  " + e.Message);
                        else
                            OnError?.Invoke("Lost ticker connection.");
                    }
                };

                // To start the receive loop in the beginning
                m_WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None).ContinueWith(callback);
            }
            catch (Exception e)
            {
                OnError?.Invoke("Error while recieving data. Message:  " + e.Message);
            }
        }

        /// <summary>
        /// Send message to socket connection
        /// </summary>
        /// <param name="Message">Message to send</param>
        public void Send(string Message)
        {
            if (m_WebSocket.State == WebSocketState.Open)
                try
                {
                    m_WebSocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(Message)), WebSocketMessageType.Text, true, CancellationToken.None).Wait();
                }
                catch (Exception e)
                {
                    OnError?.Invoke("Error while sending data. Message:  " + e.Message);
                }
        }

        /// <summary>
        /// Close the WebSocket connection
        /// </summary>
        /// <param name="Abort">If true WebSocket will not send 'Close' signal to server. Used when connection is disconnected due to netork issues.</param>
        public void Close(bool Abort = false)
        {
            if (m_WebSocket.State == WebSocketState.Open)
            {
                try
                {
                    if (Abort)
                        m_WebSocket.Abort();
                    else
                    {
                        m_WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None).Wait();
                        OnClose?.Invoke();
                    }
                }
                catch (Exception e)
                {
                    OnError?.Invoke("Error while closing connection. Message: " + e.Message);
                }
            }
        }
    }
}
