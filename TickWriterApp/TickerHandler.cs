﻿using KiteTickerLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TickWriter
{
    /// <summary>
    /// Handles Ticker related events
    /// </summary>
    public class TickerHandler
    {
        private DataflowProducerConsumer m_DataflowProducerConsumer;

        public TickerHandler(DataflowProducerConsumer dataflowProducerConsumer)
        {
            m_DataflowProducerConsumer = dataflowProducerConsumer;
        }

        /// <summary>
        /// Initializes Ticker and subscribe tokens to Zerodha
        /// </summary>
        public void StartTicker(Instruments instruments, string apiKey, string apiAccessToken)
        {
            // Create web socket connection 
            var ticker = new Ticker(apiKey, apiAccessToken);

            // Bind web socket events
            ticker.OnTick += OnTick;
            ticker.OnReconnect += OnReconnect;
            ticker.OnNoReconnect += OnNoReconnect;
            ticker.OnError += OnError;
            ticker.OnClose += OnClose;
            ticker.OnConnect += OnConnect;

            // Configure reconnection params
            ticker.EnableReconnect(Interval: 5, Retries: 50);

            // Connect to web socket
            ticker.Connect();

            // Subscribing to NIFTY50
            ticker.Subscribe(Tokens: InstrumentwiseTokenSymbolMap.InstrumentwiseTokenSymbolMapNSE[instruments.ToString()].Keys.ToArray());

            // Set mode to Full
            ticker.SetMode(Tokens: InstrumentwiseTokenSymbolMap.InstrumentwiseTokenSymbolMapNSE[instruments.ToString()].Keys.ToArray(), Mode: Constants.MODE_FULL);
        }

        /// <summary>
        /// Event triggered when ticker is connected
        /// </summary>
        private void OnConnect()
        {
            Console.WriteLine("Connected ticker");
        }

        /// <summary>
        /// Event triggered when ticker is disconnected
        /// </summary>
        private void OnClose()
        {
            Console.WriteLine("Closed ticker");
        }

        /// <summary>
        /// Event triggered when ticker encounters an error
        /// </summary>
        /// <param name="Message">Error message</param>
        private void OnError(string Message)
        {
            Console.WriteLine("Error: " + Message);
        }

        /// <summary>
        /// Event triggered when ticker is not reconnecting after failure
        /// </summary>
        private void OnNoReconnect()
        {
            Console.WriteLine("Not reconnecting");
        }

        /// <summary>
        /// Event triggered when ticker is reconnected
        /// </summary>
        private void OnReconnect()
        {
            Console.WriteLine("Reconnecting");
        }

        /// <summary>
        /// Event triggered when ticker receives a tick
        /// </summary>
        /// <param name="TickData">Tick received</param>
        private void OnTick(Tick TickData)
        {
            Console.WriteLine($"Tick Time {TickData.Timestamp} : Token {TickData.InstrumentToken}");

            // Produce data to buffer
            m_DataflowProducerConsumer.Produce(TickData);
        }
    }
}
