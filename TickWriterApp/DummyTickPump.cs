﻿using KiteTickerLibrary;
using System;

namespace TickWriter
{
    public class DummyTickPump
    {
        public static Tick GetTick(DateTime lastTradedTime)
        {
            Tick tick = new Tick();
            tick.Mode = "FULL";
            tick.InstrumentToken = 408065;
            tick.Tradable = true;
            tick.LastPrice = 1034.25m;
            tick.LastQuantity = 2;
            tick.AveragePrice = 1034.35m;
            tick.Volume = 2556476;
            tick.BuyQuantity = 224230;
            tick.SellQuantity = 335291;
            tick.Open = 1040;
            tick.High = 1041.95m;
            tick.Low = 1028.9m;
            tick.Close = 1036.05m;
            tick.Change = 0;

            tick.Bids = new DepthItem[5]
            {
                new DepthItem()
                {
                    Price = 1034.1m,
                    Quantity = 42,
                    Orders = 5
                },
                new DepthItem()
                {
                    Price = 1034.05m,
                    Quantity = 6,
                    Orders = 4
                },
                new DepthItem()
                {
                    Price = 1034,
                    Quantity = 115,
                    Orders = 10
                },
                new DepthItem()
                {
                    Price = 1033.85m,
                    Quantity = 99,
                    Orders = 1
                },
                new DepthItem()
                {
                    Price = 1033.8m,
                    Quantity = 353,
                    Orders = 1
                }
            };

            tick.Offers = new DepthItem[5]
            {
                new DepthItem()
                {
                    Price = 1034.35m,
                    Quantity = 644,
                    Orders = 7
                },
                new DepthItem()
                {
                    Price = 1034.4m,
                    Quantity = 1703,
                    Orders = 5
                },
                new DepthItem()
                {
                    Price = 1034.45m,
                    Quantity = 31,
                    Orders = 2
                },
                new DepthItem()
                {
                    Price = 1034.5m,
                    Quantity = 415,
                    Orders = 5
                },
                new DepthItem()
                {
                    Price = 1034.55m,
                    Quantity = 712,
                    Orders = 4
                }
            };

            tick.LastTradeTime = lastTradedTime;
            tick.OI = 0;
            tick.OIDayHigh = 0;
            tick.OIDayLow = 0;
            tick.Timestamp = lastTradedTime.AddSeconds(1);

            return tick;
        }

    }
}
