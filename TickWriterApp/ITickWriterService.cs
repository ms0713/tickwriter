﻿namespace TickWriter
{
    public interface ITickWriterService
    {
        void Run();
    }
}