﻿using KiteTickerLibrary;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using TickWriterLibrary;

namespace TickWriter
{

    public class TickWriterService : ITickWriterService
    {
        private readonly IConfiguration m_Configuration;
        private readonly IDatabaseData m_Db;

        public const string APIKey = "APIKey";
        public const string APIAccessToken = "APIAccessToken";

        public const string EQUITY_INSTRUMENTS_JSON = "Equities.json";
        public const string FUTURE_INSTRUMENTS_JSON = "Futures.json";

        // NOTE: Hard coded token
        static uint[] silentEquityScrips = new uint[]
                                    {   958721,947969,4445185,3353857,3932673,961793,784129,4843777,2752769,916225,
                                        2952193,269569,4278529,2708481,2089729,3785729,975873,4911105,738561,114433,
                                        895745,877057,4343041,884737,414977,873217,185345,871681,5143553,5399297,
                                        1887745,733697,4708097,1174273,523009,720897,1894657,3926273,3365633,3820033,
                                        2906881,5197313,692481,2236417,2730497,681985,3725313,678145,240641,648961,
                                        5786113,676609,3660545,2905857,4701441,617473,655873,633601,2762497,2977281,
                                        625153,3924993,8042241,584449,582913,4561409,2939649,416513,506625,4923905,
                                        6386689,2061825,428801,637185,3776001,7982337,2674433,1041153,539137,4879617,
                                        345089,592897,342017,119553,341249,340481,1850625,2513665,3378433,319233,
                                        1401601,962817,4576001,2585345,36865,1332225,299009,1895937,295169,293121,
                                        403457,291585,2012673,281601,3735553,2962689,1253889,258049,5456385,3646721,
                                        2170625,907777,6549505,502785,2622209,6192641,3529217,900609,1018881,898305,
                                        897537,4330241,953345,951809,3475713,7496705,2889473,3412993,940033,4834049,
                                        3608577,5504257,3005185,5202177,1688577,2813441,3357697,806401,6201601,3649281,
                                        758529,3431425,3802369,4464129,2748929,3374593,7577089,4516097,857857,856321,
                                        854785,851713,2383105,768513,4610817,2964481,3588865,4360193,889601,3669505,
                                        5105409,211713,1471489,486657,4369665,1459457,3831297,4376065,193793,5204225,
                                        160769,3849985,160001,3406081,779521,5582849,4314113,1256193,3460353,234497,
                                        232961,3905025,5420545,2931713,4546049,613633,3930881,320001,7452929,2029825,
                                        131329,126721,2911489,103425,98049,548865,94977,94209,1214721,579329,87297,
                                        86529,85761,3712257,1510401,67329,1436161,3691009,386049,3834113,6583809,2455041,
                                        2676993,41729,40193,6599681,1148673,2995969,2079745,2197761,2955009,2949633,
                                        2707457,102145,780289,258817,1629185,6054401,1003009,4596993,369153,2478849,
                                        492033,4307713,3912449,4756737,4259585,3031297,3394561,471297,5319169,3675137,
                                        3623425,6629633,630529,4488705,548353,2452737,306177,1316609,3382017,424961,
                                        4598529,3944705,3709441,593665,2702593,3756033,3520257,3400961,519937,2893057,
                                        511233,1152769,3826433,1076225,1718529,5332481,4752385,2983425,760833,874753,
                                        356865,359937,5181953,5561857,543745,2815745,348929,2475009,589569,1124097,
                                        2713345,324353,958465,3509761,261889,462849,173057,3343617,3832833,498945,
                                        2707713,2863105,3060993,3677697,3717889,4774913,3877377,3085313,4632577,
                                        828673,5573121,245249,5415425,3689729,3471361,1020673,316161,3039233,5051137,
                                        1276417,302337,207617,3513601,149249,2763265,534529,300545,3463169,3023105,
                                        2825473,1270529,2928385,1195009,2883073,4818433,81153,78081,4268801,3848705,
                                        4999937,4267265,56321,2747905,3811585,60417,4716033,54273,5166593,408065,
                                        6401,1805569,5633,7707649,5533185,4583169,7670273,1793,4630017,4577537,2067201,
                                        197633,3876097,5506049,5215745,183041,177665,524545,163073,999937,4492033,325121,
                                        303361,25601,3350017,375553,3031041,4451329,3861249,1552897,101121,3564801,3553281,
                                        2031617,71169,70401,189953,189185,1215745,112129,2714625,97281,4589313,175361,
                                        1790465,5013761,140033,108033,121345,803329,794369,2870273,3001089,4574465,2876417,
                                        315393,3491073,3695361,3036161,3453697,3397121,1723649,774145,1149697,441857,
                                        3484417,3920129,418049,5225729,415745,2393089,3752193,7458561,1346049,790529,
                                        2865921,3663105,2745857,389377,387841,387073,336641,1207553,3885825,225537,
                                        2885377,5097729,3771393,2800641,815617,134657,558337,3526657,1102337,837889,
                                        7712001,152321,3465729,3255297,2953217,4514561,6191105,866305,2672641,1086465,
                                        969473,860929,2992385
                                    };

        static DataflowProducerConsumer dataflow;

        /// <summary>
        /// Constructor that initializes config and DB using DI
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="db"></param>
        public TickWriterService(IConfiguration configuration, IDatabaseData db)
        {
            m_Configuration = configuration;
            m_Db = db;
        }


        /// <summary>
        /// Application flow
        /// </summary>
        public void Run()
        {
            // Initialize db connection
            m_Db.InitializeDBConnection();

            // Add Map for Equity and Futures
            InstrumentwiseTokenSymbolMap.InstrumentwiseTokenSymbolMapNSE.Add(Instruments.Equity.ToString(), new Dictionary<uint, string>());
            InstrumentwiseTokenSymbolMap.InstrumentwiseTokenSymbolMapNSE.Add(Instruments.Future.ToString(), new Dictionary<uint, string>());

            //Read list of Symbols using Instruments.json
            LoadInstruments(EQUITY_INSTRUMENTS_JSON, InstrumentwiseTokenSymbolMap.InstrumentwiseTokenSymbolMapNSE, silentEquityScrips);
            LoadInstruments(FUTURE_INSTRUMENTS_JSON, InstrumentwiseTokenSymbolMap.InstrumentwiseTokenSymbolMapNSE);

            // Create table if not exist
            m_Db.CreateTable(InstrumentwiseTokenSymbolMap.InstrumentwiseTokenSymbolMapNSE);

            // Create Producer-Consumer instance
            dataflow = DataflowProducerConsumer.GetInstance();

            // Bind consumer event to DB Manager
            dataflow.OnTickConsume += m_Db.OnConsume;

            //Start listening for data
            var consumer = dataflow.ConsumeAsync();

            //Start Tickers
            var equityHandler = new TickerHandler(dataflow);
            equityHandler.StartTicker(Instruments.Equity, m_Configuration.GetValue<string>(APIKey), m_Configuration.GetValue<string>(APIAccessToken));

            var futureHandler = new TickerHandler(dataflow);
            futureHandler.StartTicker(Instruments.Future, m_Configuration.GetValue<string>(APIKey), m_Configuration.GetValue<string>(APIAccessToken));

            //// Pump dummy tick data
            //Task.Run(() =>
            //{
            //    Tick tick = DummyTickPump.GetTick(new DateTime(2018, 01, 09, 11, 36, 08));
            //    dataflow.Produce(tick);
            //});

            Console.ReadLine();
        }

        /// <summary>
        /// Read instrument JSON file and create Instrument Token - Symbol map  
        /// </summary>
        /// <param name="instrumentFileName">JSON file for instruments</param>
        /// <param name="instrumentwiseTokenSymbolMap">Instrument Token - Symbol Map</param>
        /// <param name="silentEquityList">List of equity symbols</param>
        private void LoadInstruments(string instrumentFileName, Dictionary<string, Dictionary<uint, string>> instrumentwiseTokenSymbolMap, uint[] silentEquityList = null)
        {

            List<Instrument> instruments = null;
            try
            {
                if (System.IO.File.Exists(instrumentFileName))
                {
                    var jsonData = System.IO.File.ReadAllText(instrumentFileName);

                    // Get current date in total milliseconds format wrt 1970-01-01 
                    string currentDate = Convert.ToInt64(DateTime.Now.Subtract(new DateTime(1970, 01, 01, 00, 00, 00)).TotalMilliseconds).ToString();

                    // Replace "Expiry":null with "Expiry":123456789 (Today's date in Integer) 
                    jsonData = jsonData.Replace("\"Expiry\":null", $"\"Expiry\":{currentDate}");

                    // Replace "Expiry":"\/Date(1606329000000)\/" with "Expiry":1606329000000 (date in Integer)
                    jsonData = jsonData.Replace("\"Expiry\":\"\\/Date(", $"\"Expiry\":");
                    jsonData = jsonData.Replace(")\\/\"", "");

                    instruments = JsonSerializer.Deserialize<List<Instrument>>(jsonData, new JsonSerializerOptions() { });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Log Exception : {ex.Message}");
            }

            if (instruments == null)
                return;

            // Create Map
            CreateInstrumentTokenSymbolMap(instruments, instrumentwiseTokenSymbolMap, silentEquityList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instruments">List of instruments</param>
        /// <param name="instrumentwiseTokenSymbolMap">Instrument Token - Symbol MAp</param>
        /// <param name="silentEquityList">List of equity symbols</param>
        private void CreateInstrumentTokenSymbolMap(List<Instrument> instruments, Dictionary<string, Dictionary<uint, string>> instrumentwiseTokenSymbolMap, uint[] silentEquityList = null)
        {
            // Prepare Instrument Token - Trading Symbol Map for NSE
            foreach (var instrument in instruments)
            {
                if (silentEquityList != null)
                {    // Equity 
                    if (instrument.Exchange == Exchange.NSE.ToString() && silentEquityList.Contains(instrument.InstrumentToken) && instrument.InstrumentType == InstrumentType.EQ.ToString())
                        instrumentwiseTokenSymbolMap[Instruments.Equity.ToString()].TryAdd(instrument.InstrumentToken, instrument.TradingSymbol.Replace(' ', '_').Replace('&', '_').Replace('-', '_').Replace('3', ' '));
                }
                else
                {
                    // Future
                    if (instrument.Exchange == Exchange.NFO.ToString() && instrument.InstrumentType == InstrumentType.FUT.ToString())
                        instrumentwiseTokenSymbolMap[Instruments.Future.ToString()].TryAdd(instrument.InstrumentToken, instrument.TradingSymbol.Replace(' ', '_').Replace('&', '_').Replace('-', '_').Replace('3', ' '));
                }
            }
        }
    }
}
