﻿using KiteTickerLibrary;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TickWriter
{
    public class DataflowProducerConsumer
    {
        /// <summary>
        /// Delegate for Tick consume event
        /// </summary>
        /// <param name="TickData">Tick data</param>
        public delegate void OnTickConsumeHandler(Tick TickData);

        /// <summary>
        /// Event triggered when ticker receives a tick
        /// </summary>
        public event OnTickConsumeHandler OnTickConsume;

        private static DataflowProducerConsumer mDataflowProducerConsumer = new DataflowProducerConsumer();
        public static DataflowProducerConsumer GetInstance()
        {
            return mDataflowProducerConsumer; 
        }

        /// <summary>
        /// Shared Buffer for Producer-Consumer 
        /// </summary>
        private BufferBlock<Tick> BufferBlock = new BufferBlock<Tick>();

        // Demonstrates the production end of the producer and consumer pattern.
        public void Produce(Tick message)
        {
            // Post the result to the message block.
            BufferBlock.Post(message);
        }

        public void ProduceComplete()
        {
            // Set the target to the completed state to signal to the consumer
            // that no more data will be available.
            BufferBlock.Complete();
        }

        // Demonstrates the consumption end of the producer and consumer pattern.
        public async Task<int> ConsumeAsync()
        {
            int count = 0;

            // Read from the source buffer until the source buffer has no available output data.
            while (await BufferBlock.OutputAvailableAsync())
            {
                OnTickConsume(BufferBlock.Receive());
                count++;
            }

            return count;
        }

        public void ProcessMessage(Tick message)
        {
            
        }
    }
}
