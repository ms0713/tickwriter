﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using TickWriterLibrary;

namespace TickWriter
{
    public class TickWriter
    {
        static void Main(string[] args)
        {
            // Set up Dependency Injection here
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json",optional:false,reloadOnChange:true);

            // Create host builder here
            var host = Host.CreateDefaultBuilder()
               .ConfigureServices((context, services) =>
               {
                   services.AddSingleton<ITickWriterService, TickWriterService>();
                   services.AddSingleton<IPostgreSQLDataAccess, PostgreSQLDataAccess>();
                   services.AddSingleton<IDatabaseData, PostgreSQLData>();
               })
               .Build();

            var service = ActivatorUtilities.CreateInstance<TickWriterService>(host.Services);

            service.Run();

            Console.ReadLine();
        }

        
    }
}
