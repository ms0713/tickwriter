﻿using KiteTickerLibrary;
using System.Collections.Generic;

namespace TickWriterLibrary
{
    public interface IDatabaseData
    {
        void CreateTable(Dictionary<string, Dictionary<uint, string>> instrumentwiseTokenSymbolMap);
        void InitializeDBConnection();
        void OnConsume(Tick TickData);
    }
}