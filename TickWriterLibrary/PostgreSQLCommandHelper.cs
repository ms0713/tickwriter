﻿using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TickWriterLibrary
{
    public static class PostgreSQLCommandHelper
    {
        #region DB Command Helper Methods

        /// <summary>
        /// Returns DB EXIST command 
        /// </summary>
        /// <param name="dbName">Name of database</param>
        /// <returns></returns>
        public static string GetDBExistCheckCmdStr(string dbName)
        {
            return $@"SELECT 1 FROM pg_database WHERE datname = '{dbName}'";
        }

        /// <summary>
        /// Returns CREATE command for database creation
        /// </summary>
        /// <param name="dbName">Name of database</param>
        /// <returns></returns>
        public static string GetCreateDBCmdStr(string dbName, string userId)
        {
            return $@"CREATE DATABASE {dbName} WITH 
                                        OWNER = {userId}
                                        ENCODING = 'UTF8'
                                        CONNECTION LIMIT = -1;

                                    COMMENT ON DATABASE {dbName} IS '{dbName} database for TimescaleDB.';";
        }

        /// <summary>
        /// Returns EXTENSION command string to convert DB to TimescaleDB
        /// </summary>
        /// <returns></returns>
        public static string GetDBConversionCmdStr()
        {
            return @"CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;";
        }

        /// <summary>
        /// Get CREATE command to create table
        /// </summary>
        /// <param name="tableName">Name of table</param>
        /// <param name="columns">DB column name and data type pair</param>
        public static string GetCreateDBTableCmdStr(string tableName, Dictionary<string, NpgsqlDbType> schema)
        {
            string createTableString = $@"CREATE TABLE IF NOT EXISTS {tableName} (";

            foreach (var (columnName, datatype) in schema)
            {
                createTableString += $"{columnName}    {datatype.ToString()},";
            }

            createTableString = createTableString.Substring(0, createTableString.Length - 1);

            createTableString += ");";

            // Replace double to double precision
            createTableString = createTableString.Replace($"{NpgsqlDbType.Double}", "double precision");

            return createTableString;
        }

        /// <summary>
        /// Get command to convert table to hypertable
        /// </summary>
        /// <param name="tableName">Name of db table</param>
        public static string GetHyperTableConversionCmdStr(string tableName)
        {
            return $@"SELECT create_hypertable('{tableName}', 'time');";
        }

        /// <summary>
        /// Get INSERT command for Tick data
        /// </summary>
        /// <returns></returns>
        public static string GetInsertCmdStr(Dictionary<string, NpgsqlDbType> schema)
        {
            string cmdStr = string.Empty;

            cmdStr += $@"INSERT INTO tablename (";

            foreach (var column in schema.Keys)
            {
                cmdStr += $@"{column}, ";
            }

            // Remove last coma
            cmdStr = cmdStr.Substring(0, cmdStr.LastIndexOf(','));

            cmdStr += @") VALUES(";

            foreach (var column in schema.Keys)
            {
                cmdStr += $@"@{column}, ";
            }

            // Remove last coma
            cmdStr = cmdStr.Substring(0, cmdStr.LastIndexOf(','));

            cmdStr += @")";

            return cmdStr;
        }

        #endregion
    }
}
