﻿using KiteTickerLibrary;
using System;
using System.Collections.Generic;

namespace TickWriterLibrary
{
    public class PostgreSQLData : IDatabaseData
    {
        private const string connectionStringName = "PostgreSQL";
        private const string databaseName = "DatabaseName";
        private const string databaseUserName = "DatabaseUserName";
        private readonly IPostgreSQLDataAccess m_Db;

        public PostgreSQLData(IPostgreSQLDataAccess db)
        {
            m_Db = db;
        }

        #region Database Logic

        /// <summary>
        /// Opens Connection to PostgreSQL Server database
        /// </summary>
        public void InitializeDBConnection()
        {
            // Connect to Postgre SQL server
            m_Db.InitializeDBConnection(connectionStringName);

            // Check if DB exist
            if (!m_Db.IsDBExist(databaseName))
                // Create db and connect to it
                m_Db.CreateDatabase(connectionStringName, databaseName, databaseUserName);
            else
                // Connect to database
                m_Db.InitializeDBConnection(connectionStringName, databaseName);
        }

        /// <summary>
        /// Creates new table in database for each symbol
        /// </summary>
        /// <param name="instrumentwiseTokenSymbolMap"></param>
        public void CreateTable(Dictionary<string, Dictionary<uint, string>> instrumentwiseTokenSymbolMap)
        {
            m_Db.CreateTable(instrumentwiseTokenSymbolMap);
        } 

        #endregion

        #region Consumer Events

        /// <summary>
        /// Event call to insert data in DB
        /// </summary>
        /// <param name="TickData"></param>
        public void OnConsume(Tick TickData)
        {
            string tableName = InstrumentwiseTokenSymbolMap.GetTableName(TickData.InstrumentToken);

            if (string.IsNullOrEmpty(tableName))
                return;

            m_Db.SaveData(tableName, TickData);
        }

        #endregion

    }
}
