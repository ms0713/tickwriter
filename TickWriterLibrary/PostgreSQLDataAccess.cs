﻿using KiteTickerLibrary;
using Microsoft.Extensions.Configuration;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;

namespace TickWriterLibrary
{
    /// <summary>
    /// Database (TimescaleDB) operations handler
    /// </summary>
    public class PostgreSQLDataAccess : IPostgreSQLDataAccess
    {
        #region Private Properties

        /// <summary>
        /// Postgres SQL connection handler
        /// </summary>
        private NpgsqlConnection mNpgsqlConnection;

        /// <summary>
        /// Postgres SQL command
        /// </summary>
        private NpgsqlCommand mNpgsqlCommand;

        /// <summary>
        /// Command param list for INSERT data
        /// </summary>
        private static NpgsqlParameter[] NpgsqlParameters;

        /// <summary>
        /// Table schema with column name and its datatype
        /// NOTE: This is hard coded schema
        /// </summary>
        private static Dictionary<string, string> TableSchema2 = new Dictionary<string, string>()
            {
                {"Mode","varchar(10)" },
                {"InstrumentToken","bigint" },
                {"Tradable","bool" },
                {"LastPrice","double precision" },
                {"LastQuantity","bigint" },
                {"AveragePrice","double precision" },
                {"Volume","bigint" },
                {"BuyQuantity","bigint" },
                {"SellQuantity","bigint" },
                {"Open","double precision" },
                {"High","double precision" },
                {"Low","double precision" },
                {"Close","double precision" },
                {"Change","double precision" },
                {"Bid1Quantity","bigint" },
                {"Bid1Price","double precision" },
                {"Bid1Orders","bigint" },
                {"Bid2Quantity","bigint" },
                {"Bid2Price","double precision" },
                {"Bid2Orders","bigint" },
                {"Bid3Quantity","bigint" },
                {"Bid3Price","double precision" },
                {"Bid3Orders","bigint" },
                {"Bid4Quantity","bigint" },
                {"Bid4Price","double precision" },
                {"Bid4Orders","bigint" },
                {"Bid5Quantity","bigint" },
                {"Bid5Price","double precision" },
                {"Bid5Orders","bigint" },
                {"Offer1Quantity","bigint" },
                {"Offer1Price","double precision" },
                {"Offer1Orders","bigint" },
                {"Offer2Quantity","bigint" },
                {"Offer2Price","double precision" },
                {"Offer2Orders","bigint" },
                {"Offer3Quantity","bigint" },
                {"Offer3Price","double precision" },
                {"Offer3Orders","bigint" },
                {"Offer4Quantity","bigint" },
                {"Offer4Price","double precision" },
                {"Offer4Orders","bigint" },
                {"Offer5Quantity","bigint" },
                {"Offer5Price","double precision" },
                {"Offer5Orders","bigint" },
                {"LastTradeTime", "TIMESTAMP WITHOUT TIME ZONE" },
                {"OI","bigint" },
                {"OIDayHigh","bigint" },
                {"OIDayLow","bigint" },
                {"Timestamp", "TIMESTAMP WITHOUT TIME ZONE" }
            };

        /// <summary>
        /// Table schema with column name and its datatype
        /// NOTE: This is hard coded schema
        /// </summary>
        private static Dictionary<string, NpgsqlDbType> TableSchema = new Dictionary<string, NpgsqlDbType>()
            {
                {"time", NpgsqlDbType.Timestamp },
                {"Mode", NpgsqlDbType.Varchar},
                {"InstrumentToken", NpgsqlDbType.Bigint },
                {"Tradable", NpgsqlDbType.Boolean },
                {"LastPrice", NpgsqlDbType.Double },
                {"LastQuantity", NpgsqlDbType.Bigint },
                {"AveragePrice", NpgsqlDbType.Double },
                {"Volume", NpgsqlDbType.Bigint },
                {"BuyQuantity", NpgsqlDbType.Bigint },
                {"SellQuantity", NpgsqlDbType.Bigint },
                {"Open", NpgsqlDbType.Double },
                {"High", NpgsqlDbType.Double },
                {"Low", NpgsqlDbType.Double },
                {"Close", NpgsqlDbType.Double },
                {"Change", NpgsqlDbType.Double },
                {"Bid1Quantity", NpgsqlDbType.Bigint },
                {"Bid1Price", NpgsqlDbType.Double },
                {"Bid1Orders", NpgsqlDbType.Bigint },
                {"Bid2Quantity", NpgsqlDbType.Bigint },
                {"Bid2Price", NpgsqlDbType.Double },
                {"Bid2Orders", NpgsqlDbType.Bigint },
                {"Bid3Quantity", NpgsqlDbType.Bigint },
                {"Bid3Price", NpgsqlDbType.Double },
                {"Bid3Orders", NpgsqlDbType.Bigint },
                {"Bid4Quantity", NpgsqlDbType.Bigint },
                {"Bid4Price", NpgsqlDbType.Double },
                {"Bid4Orders", NpgsqlDbType.Bigint },
                {"Bid5Quantity", NpgsqlDbType.Bigint },
                {"Bid5Price", NpgsqlDbType.Double },
                {"Bid5Orders", NpgsqlDbType.Bigint },
                {"Offer1Quantity", NpgsqlDbType.Bigint },
                {"Offer1Price", NpgsqlDbType.Double },
                {"Offer1Orders", NpgsqlDbType.Bigint },
                {"Offer2Quantity", NpgsqlDbType.Bigint },
                {"Offer2Price", NpgsqlDbType.Double },
                {"Offer2Orders", NpgsqlDbType.Bigint },
                {"Offer3Quantity", NpgsqlDbType.Bigint },
                {"Offer3Price", NpgsqlDbType.Double },
                {"Offer3Orders", NpgsqlDbType.Bigint },
                {"Offer4Quantity", NpgsqlDbType.Bigint },
                {"Offer4Price", NpgsqlDbType.Double },
                {"Offer4Orders", NpgsqlDbType.Bigint },
                {"Offer5Quantity", NpgsqlDbType.Bigint },
                {"Offer5Price", NpgsqlDbType.Double },
                {"Offer5Orders", NpgsqlDbType.Bigint },
                {"LastTradeTime", NpgsqlDbType.Timestamp },
                {"OI", NpgsqlDbType.Bigint },
                {"OIDayHigh", NpgsqlDbType.Bigint },
                {"OIDayLow", NpgsqlDbType.Bigint },
                {"Timestamp", NpgsqlDbType.Timestamp }
            };

        /// <summary>
        /// Configuration for DI
        /// </summary>
        private readonly IConfiguration m_Configuration;

        #endregion

        #region Public Properties

        /// <summary>
        /// Instrument Token - Trading Symbol mapping for NSE
        /// </summary>
        public static Dictionary<uint, string> TokenSymbolMapNSE = new Dictionary<uint, string>();

        /// <summary>
        /// Table schema with column name and its datatype
        /// NOTE: This is hard coded schema
        /// </summary>
        public static Dictionary<string, NpgsqlDbType> TableSchemaTest = new Dictionary<string, NpgsqlDbType>()
            {
                {"time", NpgsqlDbType.Timestamp },
                {"Mode", NpgsqlDbType.Varchar},
                {"InstrumentToken", NpgsqlDbType.Bigint },
                {"Tradable", NpgsqlDbType.Boolean },
                {"LastPrice", NpgsqlDbType.Double },
                {"LastQuantity", NpgsqlDbType.Bigint },
                {"AveragePrice", NpgsqlDbType.Double },
                {"Volume", NpgsqlDbType.Bigint },
                {"BuyQuantity", NpgsqlDbType.Bigint },
                {"SellQuantity", NpgsqlDbType.Bigint },
                {"Open", NpgsqlDbType.Double },
                {"High", NpgsqlDbType.Double },
                {"Low", NpgsqlDbType.Double },
                {"Close", NpgsqlDbType.Double },
                {"Change", NpgsqlDbType.Double },
                {"Bid1Quantity", NpgsqlDbType.Bigint },
                {"Bid1Price", NpgsqlDbType.Double },
                {"Bid1Orders", NpgsqlDbType.Bigint },
                {"Bid2Quantity", NpgsqlDbType.Bigint },
                {"Bid2Price", NpgsqlDbType.Double },
                {"Bid2Orders", NpgsqlDbType.Bigint },
                {"Bid3Quantity", NpgsqlDbType.Bigint },
                {"Bid3Price", NpgsqlDbType.Double },
                {"Bid3Orders", NpgsqlDbType.Bigint },
                {"Bid4Quantity", NpgsqlDbType.Bigint },
                {"Bid4Price", NpgsqlDbType.Double },
                {"Bid4Orders", NpgsqlDbType.Bigint },
                {"Bid5Quantity", NpgsqlDbType.Bigint },
                {"Bid5Price", NpgsqlDbType.Double },
                {"Bid5Orders", NpgsqlDbType.Bigint },
                {"Offer1Quantity", NpgsqlDbType.Bigint },
                {"Offer1Price", NpgsqlDbType.Double },
                {"Offer1Orders", NpgsqlDbType.Bigint },
                {"Offer2Quantity", NpgsqlDbType.Bigint },
                {"Offer2Price", NpgsqlDbType.Double },
                {"Offer2Orders", NpgsqlDbType.Bigint },
                {"Offer3Quantity", NpgsqlDbType.Bigint },
                {"Offer3Price", NpgsqlDbType.Double },
                {"Offer3Orders", NpgsqlDbType.Bigint },
                {"Offer4Quantity", NpgsqlDbType.Bigint },
                {"Offer4Price", NpgsqlDbType.Double },
                {"Offer4Orders", NpgsqlDbType.Bigint },
                {"Offer5Quantity", NpgsqlDbType.Bigint },
                {"Offer5Price", NpgsqlDbType.Double },
                {"Offer5Orders", NpgsqlDbType.Bigint },
                {"LastTradeTime", NpgsqlDbType.Timestamp },
                {"OI", NpgsqlDbType.Bigint },
                {"OIDayHigh", NpgsqlDbType.Bigint },
                {"OIDayLow", NpgsqlDbType.Bigint },
                {"Timestamp", NpgsqlDbType.Timestamp }
            };

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor that initializes required store and configuration
        /// </summary>
        public PostgreSQLDataAccess(IConfiguration configuration)
        {
            m_Configuration = configuration;

            // Create SQL parameters list
            AddSqlCmdParameters();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize PostgreSQL server/database connection
        /// </summary>
        public void InitializeDBConnection(string connectionStringName, string databaseName = null)
        {
            string connectionString = $"{m_Configuration.GetConnectionString(connectionStringName)}";
            if (databaseName is not null)
            {
                connectionString += $";Database={databaseName}";
            }

            try
            {
                // Create PostgreSQL server connection
                mNpgsqlConnection = new NpgsqlConnection(connectionString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Log Exception : {ex.Message}");
            }
            finally
            {
                if (databaseName is null)
                {
                    mNpgsqlConnection.Close();
                }
            }
        }


        /// <summary>
        /// Returns true if DB already exists
        /// </summary>
        /// <param name="dbName">Name of database</param>
        /// <returns></returns>
        public bool IsDBExist(string dbName)
        {
            return ExecuteSqlScalar(PostgreSQLCommandHelper.GetDBExistCheckCmdStr(dbName)) == 1;
        }

        /// <summary>
        /// Creates new database 
        /// </summary>
        /// <param name="dbName">Name of database</param>
        public void CreateDatabase(string connectionStringName, string dbName, string userName)
        {
            string userId = m_Configuration.GetValue<string>(userName);
            string database = m_Configuration.GetValue<string>(dbName);

            // Create new database
            ExecuteSqlNonQuery(PostgreSQLCommandHelper.GetCreateDBCmdStr(database, userId));

            // Connect to newly created db
            InitializeDBConnection(connectionStringName, database);

            // Convert to TimescaleDB
            ExecuteSqlNonQuery(PostgreSQLCommandHelper.GetDBConversionCmdStr());
        }

        /// <summary>
        /// Creates new table
        /// </summary>
        /// <param name="tableName">Name of table</param>
        /// <param name="columns">Column name-datatype pair</param>
        public void CreateTable(Dictionary<string, Dictionary<uint, string>> instrumentwiseTokenSymbolMap)
        {
            int result = 0;
            // For each instrument type 
            foreach (var instrument in instrumentwiseTokenSymbolMap.Values)
            {
                Dictionary<uint, string> tokenSymbolMap = instrument;

                // For each token 
                foreach (var symbol in tokenSymbolMap.Values)
                {
                    // Create table
                    result = ExecuteSqlNonQuery(PostgreSQLCommandHelper.GetCreateDBTableCmdStr(symbol, TableSchema));

                    // New table created, then convert it to hypertable
                    if (result != -1)
                        // Convert table to hypertable
                        ExecuteSqlNonQuery(PostgreSQLCommandHelper.GetHyperTableConversionCmdStr(symbol));
                }
            }
        }

        /// <summary>
        /// Save data to Database
        /// </summary>
        /// <param name="tableName">table name where data will be saved</param>
        /// <param name="data">Data to be saved</param>
        public void SaveData(string tableName, Tick data)
        {
            // Write to DB
            ExecuteSqlInsert(PostgreSQLCommandHelper.GetInsertCmdStr(TableSchema).Replace("tablename", tableName), isTickInsert: true, tick: data);
        }

        #endregion

        #region Initialize Data Access

        /// <summary>
        /// Initializes SQL command parameters list
        /// </summary>
        private void AddSqlCmdParameters()
        {
            NpgsqlParameters = new NpgsqlParameter[TableSchema.Count];
            int count = 0;
            foreach (var (columnName, datatype) in TableSchema)
            {
                NpgsqlParameters[count] = new NpgsqlParameter($@"@{columnName}", datatype);
                count++;
            }
        }

        /// <summary>
        /// Set INSERT parameters values
        /// </summary>
        /// <param name="tick">Tick data to insert</param>
        private void SetSqlCmdParameterValues(Tick tick)
        {
            int count = 0;

            NpgsqlParameters[count++].Value = tick.Timestamp;
            NpgsqlParameters[count++].Value = tick.Mode;
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.InstrumentToken);
            NpgsqlParameters[count++].Value = tick.Tradable;
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.LastPrice);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.LastQuantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.AveragePrice);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Volume);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.BuyQuantity);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.SellQuantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Open);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.High);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Low);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Close);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Change);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[0].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Bids[0].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[0].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[1].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Bids[1].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[1].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[2].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Bids[2].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[2].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[3].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Bids[3].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[3].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[4].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Bids[4].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Bids[4].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[0].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Offers[0].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[0].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[1].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Offers[1].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[1].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[2].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Offers[2].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[2].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[3].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Offers[3].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[3].Orders);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[4].Quantity);
            NpgsqlParameters[count++].Value = Convert.ToDouble(tick.Offers[4].Price);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.Offers[4].Orders);
            NpgsqlParameters[count++].Value = tick.LastTradeTime;
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.OI);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.OIDayHigh);
            NpgsqlParameters[count++].Value = Convert.ToInt64(tick.OIDayLow);
            NpgsqlParameters[count++].Value = tick.Timestamp;
        }


        #endregion

        #region SQL Methods

        /// <summary>
        /// Executes SQL command for database
        /// </summary>
        /// <param name="cmdString">Command to execute</param>
        /// <returns></returns>
        private int ExecuteSqlScalar(string cmdString)
        {
            int count = 0;
            try
            {
                mNpgsqlConnection.Open();

                mNpgsqlCommand = new NpgsqlCommand(cmdString, mNpgsqlConnection);
                var result = mNpgsqlCommand.ExecuteScalar();

                if (result != null)
                    count = (int)result;

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Log Exception : {ex.Message}");
            }
            finally
            {
                mNpgsqlConnection.Close();
            }

            return count;
        }

        /// <summary>
        /// Executes SQL command for database
        /// </summary>
        /// <param name="cmdString">Command to execute</param>
        /// <returns></returns>
        private int ExecuteSqlNonQuery(string cmdString)
        {
            int count = 0;
            try
            {
                mNpgsqlConnection.Open();

                mNpgsqlCommand = new NpgsqlCommand(cmdString, mNpgsqlConnection);
                count = mNpgsqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Log Exception : {ex.Message}");
            }
            finally
            {
                mNpgsqlConnection.Close();
            }

            return count;
        }

        /// <summary>
        /// Executes SQL command for database
        /// </summary>
        /// <param name="cmdString">Command to execute</param>
        /// <returns></returns>
        private int ExecuteSqlInsert(string cmdString, bool isTickInsert = false, Tick tick = default(Tick))
        {
            int count = 0;
            try
            {
                mNpgsqlConnection.Open();

                mNpgsqlCommand = new NpgsqlCommand(cmdString, mNpgsqlConnection);

                if (isTickInsert)
                {
                    // Set parameters values from Tick data
                    SetSqlCmdParameterValues(tick);

                    // Add param collection to command
                    if (NpgsqlParameters != null)
                    {
                        foreach (var param in NpgsqlParameters)
                        {
                            mNpgsqlCommand.Parameters.Add(param);
                        }
                    }
                }

                count = mNpgsqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Log Exception : {ex.Message} for {cmdString}");
            }
            finally
            {
                mNpgsqlCommand.Parameters.Clear();
                mNpgsqlConnection.Close();
            }

            return count;
        }

        /// <summary>
        /// Fetches data from database 
        /// </summary>
        /// <param name="cmdString">Commandt to get data</param>
        /// <returns></returns>
        private NpgsqlDataReader GetDataReader(string cmdString)
        {
            NpgsqlDataReader dataReader = null;
            try
            {
                mNpgsqlConnection.Open();

                mNpgsqlCommand = new NpgsqlCommand(cmdString, mNpgsqlConnection);
                dataReader = mNpgsqlCommand.ExecuteReader();

                // TODO: Process data here
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Log Exception : {ex.Message}");
            }
            finally
            {
                mNpgsqlConnection.Close();
            }

            return dataReader;
        }

        #endregion

    }
}
