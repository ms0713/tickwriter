﻿using KiteTickerLibrary;
using System.Collections.Generic;

namespace TickWriterLibrary
{
    public interface IPostgreSQLDataAccess
    {
        void CreateDatabase(string connectionStringName, string dbName, string userName);
        void CreateTable(Dictionary<string, Dictionary<uint, string>> instrumentwiseTokenSymbolMap);
        void InitializeDBConnection(string connectionStringName, string databaseName = null);
        bool IsDBExist(string dbName);
        void SaveData(string tableName, Tick data);
    }
}